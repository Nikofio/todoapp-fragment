package ereale.uninsubria.disp_mobili.todoapp;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TodoItem {
    private String todo;
    private GregorianCalendar createOn; // la data di creazione del task

    public TodoItem(String todo) {
        super();
        this.todo = todo;
        this.createOn = new GregorianCalendar();
    }
    @Override
    public String toString() {
        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).format(createOn.getTime());
        return currentDate + ":\n >> " + todo;
    }
}
