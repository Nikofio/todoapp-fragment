package ereale.uninsubria.disp_mobili.todoapp;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private Button listBtn;
    private Button gridBtn;

    private EditText editText;
    private Button addButton;
    private ArrayList<TodoItem> todoItems;
    private ArrayAdapter<TodoItem> adapter;


    private ListViewFragment listViewFragment;
    private GridViewFragment gridViewFragment;

    private DBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.input_insert_todo);
        addButton = (Button) findViewById(R.id.button_add);

        todoItems = new ArrayList<TodoItem>();
        adapter = new ArrayAdapter<TodoItem>(this, android.R.layout.simple_list_item_1, todoItems);

        listViewFragment = new ListViewFragment();
        gridViewFragment = new GridViewFragment();

        listBtn = findViewById(R.id.listButton);
        gridBtn = findViewById(R.id.gridButton);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, listViewFragment).commit();

        final View listView;

        //DATABASE
        dbAdapter = DBAdapter.getInstance(this);    //Apri connessione

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String todo = editText.getText().toString();
                if (todo.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Empty ToDo string", Toast.LENGTH_LONG).show();
                    return;
                }
                TodoItem newTodo = new TodoItem(todo);
                todoItems.add(0, newTodo);
                adapter.notifyDataSetChanged();
                // clear text field
                editText.setText("");
                // hide the softkey
                InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
                editText.clearFocus();
                listViewFragment.setAdapter(adapter);
                gridViewFragment.setAdapter(adapter);
            }
        });
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.listButton)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment, listViewFragment).commit();
        } else if (view == findViewById(R.id.gridButton)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment, gridViewFragment).commit();
        } else if (view == view.findViewById(R.id.prova)) {
            Toast.makeText(getApplicationContext(), "HAI SCHIACCIATO PROVA", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onResule(){
        super.onResume();
        //Log.v(TAG, "onResume");
        dbAdapter.open();
        fillData();
    }
}
