package ereale.uninsubria.disp_mobili.todoapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListViewFragment extends Fragment {
    private View v;
    private ListView listView = null;
    ArrayAdapter<TodoItem> adapter = null;

    public ListViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_list_view, container, false);
        // Inflate the layout for this fragment
        listView = v.findViewById(R.id.list_view);
        if (adapter != null)
            listView.setAdapter(adapter);

        //final Button button = (Button) v.findViewById(R.id.prova);

       /* button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setText("CIAO");
                Toast.makeText(getActivity(), "CLICCATO!!!!!!!!!!!!", Toast.LENGTH_SHORT).show();
            }
        }); */

        return v;
    }

    public void setAdapter(ArrayAdapter<TodoItem> adapter) {
        this.adapter = adapter;
        if (listView != null)
            listView.setAdapter(adapter);
    }


}
