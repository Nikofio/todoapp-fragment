package ereale.uninsubria.disp_mobili.todoapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class GridViewFragment extends Fragment {
    private View v;
    private GridView gridView = null;
    ArrayAdapter<TodoItem> adapter = null;

    public GridViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_grid_view, container, false);
        // Inflate the layout for this fragment
        gridView = v.findViewById(R.id.grid_view);
        if (adapter != null)
            gridView.setAdapter(adapter);
        return v;
    }

    public void setAdapter(ArrayAdapter<TodoItem> adapter) {
        this.adapter = adapter;
        if (gridView != null)
            gridView.setAdapter(adapter);
    }

}
